/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema16video31;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class OutputStreamApp {

    public void escribirPorOutputStream() throws IOException {
        // byte a byte
        long ini = System.nanoTime();
        // String texto = "Suscribete al canal";
        InputStream fuente = null;
        byte[] buffer = new byte[1024 * 8];
        try (OutputStream destino = new FileOutputStream("mitoFile.gif")) {
            fuente = new FileInputStream("suscribete.gif");
            int byteRead;
            while ((byteRead = fuente.read(buffer)) != -1) {
                destino.write(buffer, 0, byteRead);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (fuente != null) {
                fuente.close();
            }
        }
        long fin = System.nanoTime();
        System.out.println("Tiempo OutputStream " + (fin - ini));
    }

    public static void main(String[] args) throws IOException {
        final OutputStreamApp app = new OutputStreamApp();

        Runnable r1 = new Runnable() {

            @Override
            public void run() {
                try {
                    app.escribirPorOutputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        };
        Thread hilo1 = new Thread(r1);
		hilo1.start();
    }
}
