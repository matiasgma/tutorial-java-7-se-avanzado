/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema6video11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Mat�as
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<Persona> lista = new ArrayList<Persona>();

        lista.add(new Persona(1, "Jaime", 25));
        lista.add(new Persona(1, "Mito", 15));
        lista.add(new Persona(1, "Code", 29));
        lista.add(new Persona(1, "AAA", 24));

        //Collections.sort(lista, new NombreComparator());
        Collections.sort(lista, new Comparator<Persona>() {

            public int compare(Persona per1, Persona per2) {
               int rpta = 0;
               if(per1.getEdad() > per2.getEdad()){
                   rpta = 555;
               }else if(per1.getEdad() < per2.getEdad()){
                   rpta = -1;
            }else{
                   rpta = 0;
                   
               }
               return rpta;
            }
        });
        

        for (Persona p : lista) {

            System.out.println(p.getNombre());
        }

    }
}
