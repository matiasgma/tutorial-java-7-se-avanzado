/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema15video27;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

/**
 *
 * @author Mat�as
 */
public class AppStatement {

    private Connection con = null;

    public void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = null;
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbtest", "root", "123");
            System.out.println("Conexi�n exitosa");
        } catch (Exception e) {
            System.out.println("Error de conexi�n");
        }
    }

    public void desconectar() throws SQLException {
        if (con != null) {
            con.close();
        }
    }

    public void registrarCallableStatement(Persona per) {
        try {
            String sql = "{call spTest(?,?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.setString(1, per.getNombre());
            cs.setString(2, per.getPass());
            cs.execute();
            cs.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void listarCallableStatement() {
        try {
            String sql = "{call spListar()}";
            CallableStatement cs = con.prepareCall(sql);
            cs.execute();

            ResultSet rs = cs.getResultSet();

            while (rs.next()) {
                System.out.print(rs.getInt("id"));
                System.out.print(rs.getString("nombre"));
                System.out.println(rs.getString("pass"));
            }
            cs.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void listarOutCallableStatement(Persona per) {
        try {
            String sql = "{call spSalidaID(?,?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.setString(1, per.getNombre());
            cs.registerOutParameter("idParam", Types.INTEGER);
            cs.execute();

            int idSalida = cs.getInt("idParam");
            System.out.println("El codigo obtenido de salida es: " + idSalida);
            cs.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws SQLException {
        AppStatement app = new AppStatement();
        app.conectar();

        //app.registrarCallableStatement(new Persona("Mito", "25"));
        app.listarOutCallableStatement(new Persona("Mito", "25"));

        app.desconectar();

    }
}
