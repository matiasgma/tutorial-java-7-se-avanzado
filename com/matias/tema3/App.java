/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema3;

/**
 *
 * @author Mat�as
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String... mitocde) {

        //PaisDAOImpl dao = new PaisDAOImpl();
        //Con Singleton correcto
        
        PaisDAOImpl dao = PaisDAOImpl.getInstance();
        
        for (Object obj : dao.getPaises()) {
            System.out.println(((Pais) obj).getNombre());
        }
        
        for (Object obj : dao.getPaises()) {
            System.out.println(((Pais) obj).getNombre());
        }
        
        PaisDAOImpl daoi = PaisDAOImpl.getInstance();
        for (Object obj : daoi.getPaises()) {
            System.out.println(((Pais) obj).getNombre());
        }
    }
}
