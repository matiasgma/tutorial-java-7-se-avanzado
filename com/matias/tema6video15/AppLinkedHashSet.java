/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema6video15;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Mat�as
 */
public class AppLinkedHashSet {
    
    public static void main(String[] args) {
		Set<Persona> lista = new LinkedHashSet<Persona>();		
		lista.add(new Persona(1,"MitoCode",22));
		lista.add(new Persona(1,"Mito",22));
		lista.add(new Persona(1,"Code",22));
		lista.add(new Persona(1,"Code",22));
		lista.add(new Persona(1,"MitoCode",25));
		lista.add(new Persona(1,"AAA",22));
		
		for(Persona per : lista){
			System.out.println(per.getEdad()+"-" + per.getNombre());
		}
	}
}
