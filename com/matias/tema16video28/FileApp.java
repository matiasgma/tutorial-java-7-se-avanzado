/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema16video28;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Mat�as
 */
public class FileApp {

    public static void main(String[] args) throws IOException {

        File f1 = new File("Archivos");

        File f2 = new File("Archivos", "SubArchivos");

        File f3 = new File(f1, "mitocode.txt");

        System.out.println(f3.createNewFile());

    }
}
