/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.matias.tema15video25;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Mat�as
 */
public class AppStatement {

    private Connection con = null;

    public void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = null;
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbtest", "root", "123");
            System.out.println("Conexi�n exitosa");
        } catch (Exception e) {
            System.out.println("Error de conexi�n");
        }
    }

    public void desconectar() throws SQLException {
        if (con != null) {
            con.close();
        }
    }

    public boolean leerStatement(Persona per) throws SQLException {

        boolean rpta = false;

        try (Statement st = con.createStatement()) {
            String sql = "SELECT * FROM test  WHERE nombre = '" + per.getNombre() + "' and pass = '" + per.getPass() + "'";
            System.out.println("Query =>" + sql);
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                System.out.println("existen datos");
                rpta = true;
            } else {
                System.out.println("no existen datos");
            }
            System.out.println("Consulta exitosa");

        }
        return rpta;
    }

    public static void main(String[] args) throws SQLException {
        AppStatement app = new AppStatement();
        app.conectar();
        boolean rpta = app.leerStatement(new Persona("ABC","25'OR 'M'= 'M"));
        app.desconectar();

        if (rpta) {
            System.out.println("Verificaci�n correcta, ingresando al sistema");
            
        }else{
            System.out.println("Credenciales incorrectas, acceso denegado");
        }
    }
}
