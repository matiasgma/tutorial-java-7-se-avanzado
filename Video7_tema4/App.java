/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Video7_tema4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mat�as
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Clase<String,Integer,String,Double> c = new Clase<String,Integer,String,Double>("Mat�as",20,"mgma",20.0);
        c.mostrarTipo();
        
        List<Clase<String,Integer,String,String>> lista = new ArrayList<Clase<String,Integer,String,String>>();
        
        lista.add(new Clase<String,Integer,String,String>("Matias",20,"Molina","Hola"));
        
        for(Clase<String,Integer,String,String> cl : lista){
            
            cl.mostrarTipo();
        }
    }
    
}
